package models

import (
    "labix.org/v2/mgo"
    "labix.org/v2/mgo/bson"
    "github.com/revel/revel"
)

type (
    DisplaySetting struct {
        Size               []string         `bson:"size"`     // for picture
        Format             string           `bson:"format"`   // for date
        Length             string           `bson:"length"`   // for long text
        Label              string           `bson:"label"`    // boolen to show or hide the label
    }
    DisplayField struct {
        Ref_field          bson.ObjectId    `bson:"ref_field"`
        DisplaySettings    DisplaySetting   `bson:"settings"`
    }
    Display struct {
        Model                               `bson:",inline"`
        Name               string           `bson:"name"`
        Description        string           `bson:"description"`
        Ref_content_type   bson.ObjectId    `bson:"ref_content_type"`
        Fields             []DisplayField   `bson:"fields"`
    }
)

/**
 * Get a display by id.
 */
func GetDisplayByObjectId(s *mgo.Session, Id bson.ObjectId) *Display {
    d := new(Display)
    Collection(d, s).FindId(Id).One(d)
    return d
}

/**
 * Get all displays.
 */
func GetAllDisplays(s *mgo.Session) []*Display {
    displays := []*Display{}
    d := new(Display)
    Collection(d, s).Find(nil).All(&displays)
    return displays
}

/**
 * Create a display instance.
 */
func (d *Display) Save(s *mgo.Session) error {
    coll := Collection(d, s)
    _, err := coll.Upsert(bson.M{"_id": d.Id}, d)
    if err != nil {
        revel.WARN.Printf("Unable to save display: %v error %v", d, err)
    }
    return err
}

/**
 * Delete the given display instance by the given id.
 */
func (d *Display) Delete(s *mgo.Session) error {
    coll := Collection(d, s)
    err := coll.RemoveId(d.Id)
    if err != nil {
        revel.WARN.Printf("Unable to delete display: %v error %v", d, err)
    }
    return err
}