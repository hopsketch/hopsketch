package models

import (
    "labix.org/v2/mgo"
    "labix.org/v2/mgo/bson"
    "github.com/revel/revel"
)

type User struct {
    Model                            `bson:",inline"`
    Email          string            `bson:"email"`
    Ref_projects   []bson.ObjectId   `bson:"ref_projects"`
}

/**
 * Get an user by id.
 */
func GetUserByObjectId(s *mgo.Session, Id bson.ObjectId) *User {
    u := new(User)
    Collection(u, s).FindId(Id).One(u)
    return u
}

/**
 * Get all users.
 */
func GetAllUsers(s *mgo.Session) []*User {
    users := []*User{}
    u := new(User)
    Collection(u, s).Find(nil).All(&users)
    return users
}

/**
 * Create an user instance.
 */
func (u *User) Save(s *mgo.Session) error {
    coll := Collection(u, s)
    _, err := coll.Upsert(bson.M{"_id": u.Id}, u)
    if err != nil {
        revel.WARN.Printf("Unable to save user: %v error %v", u, err)
    }
    return err
}

/**
 * Delete the given user instance by the given id.
 */
func (u *User) Delete(s *mgo.Session) error {
    coll := Collection(u, s)
    err := coll.RemoveId(u.Id)
    if err != nil {
        revel.WARN.Printf("Unable to delete user: %v error %v", u, err)
    }
    return err
}