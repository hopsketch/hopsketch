package models

import (
    "bitbucket.org/hopsketch/hopsketch/app"
    "github.com/revel/revel"
    "labix.org/v2/mgo"
    "labix.org/v2/mgo/bson"
    "reflect"
)

var (
    Collections map[string]string
)

// Empty struct to embed in models that will provide application default funcs.
type Model struct {
    Id   bson.ObjectId   `bson:"_id,omitempty"`
}

func Collection(m interface{}, s *mgo.Session) *mgo.Collection {
    typ := reflect.TypeOf(m).Elem()
    n := typ.Name()

    var found bool
    var c string
    if c, found = revel.Config.String("hopsketch.db.collection." + n); !found {
        c = n
    }
    return s.DB(app.DB).C(c)
}
