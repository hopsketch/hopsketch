package app

import (
	"github.com/jgraham909/revmgo"
	"github.com/revel/revel"
	"strings"
	"github.com/cbonello/revel-csrf"
	"github.com/iassic/revel-modz/modules/grunt"
)

func init() {
	revel.OnAppStart(revmgo.AppInit)
	revel.OnAppStart(AppInit)
	// Filters is the default set of global filters.
	revel.Filters = []revel.Filter{
		revel.PanicFilter,             // Recover from panics and display an error page instead.
		revel.RouterFilter,            // Use the routing table to select the right Action
		revel.FilterConfiguringFilter, // A hook for adding or removing per-Action filters.
		revel.ParamsFilter,            // Parse parameters into Controller.Params.
		revel.SessionFilter,           // Restore and write the session cookie.
		revel.FlashFilter,             // Restore and write the flash cookie.
		csrf.CSRFFilter,               // CSRF prevention.
		revel.ValidationFilter,        // Restore kept validation errors and save new ones from cookie.
		revel.I18nFilter,              // Resolve the requested language
		HeaderFilter,                  // Add some security based headers
		revel.InterceptorFilter,       // Run interceptors around the action.
		revel.CompressFilter,          // Compress the result.
		revel.ActionInvoker,           // Invoke the action.
	}

	// register startup functions with OnAppStart
	// ( order dependent )
	// revel.OnAppStart(InitDB())
	// revel.OnAppStart(FillCache())

	revel.TemplateFuncs["join"] = func(a []string, sep string) string {
		return strings.Join(a, sep)
	}

	// setup grunt asset compilers
	revel.OnAppStart(func() {
		if revel.MainWatcher != nil {
			appPath := revel.BasePath
			for _, gruntCompiler := range compilers {
				path := filepath.Join(appPath, gruntCompiler.Path)
				revel.INFO.Printf("Listening: %q\n", path)
				revel.MainWatcher.Listen(gruntCompiler, path)
			}
		}
	})

}

// TODO turn this into revel.HeaderFilter
// should probably also have a filter for CSRF
// not sure if it can go in the same filter or not
var HeaderFilter = func(c *revel.Controller, fc []revel.Filter) {
	// Add some common security headers
	c.Response.Out.Header().Add("X-Frame-Options", "SAMEORIGIN")
	c.Response.Out.Header().Add("X-XSS-Protection", "1; mode=block")
	c.Response.Out.Header().Add("X-Content-Type-Options", "nosniff")

	fc[0](c, fc[1:]) // Execute the next filter stage.
}

var compilers = []*grunt.GruntCompiler{
	grunt.NewCompiler("Gruntfile.js", "Gruntfile.js", "default"),

	// for examples:
	// grunt.NewCompiler("Foundation JS", "app/assets/js/foundation", "uglify:foundation_js"),
	// grunt.NewCompiler("Foundation SASS", "app/assets/sass/foundation", "sass:foundation_css"),
	// grunt.NewCompiler("Foundation SASS", "app/assets/sass/foundation_custom.scss", "sass:foundation_css"),

	// grunt.NewCompiler("sample JS", "app/assets/js/sample", "uglify:sample_js"),
	// grunt.NewCompiler("sample SASS", "app/assets/sass/sample", "sass:sample_css"),
	// grunt.NewCompiler("sample SASS", "app/assets/sass/sample.scss", "sass:sample_css"),
}
