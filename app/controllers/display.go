package controllers

import (
    "github.com/revel/revel"
    models "bitbucket.org/hopsketch/hopsketch/app/models"
    "labix.org/v2/mgo/bson"
    "encoding/json"
)

type Display struct {
    App
}

/**
 * List all displays.
 */
func (d Display) List() revel.Result {
    displays := models.GetAllDisplays(d.MongoSession)
    d.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
    return d.RenderJson(displays)
}

/**
 * Get a specific display by id.
 */
func (d Display) Get(id bson.ObjectId) revel.Result {
    if id.Hex() != "" {
        display := models.GetDisplayByObjectId(d.MongoSession, id)
        d.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
        return d.RenderJson(display)
    }
    return d.NotFound("Invalid display Id.")
}

/**
 * Delete a specific display by id.
 */
func (d Display) Delete(id bson.ObjectId) revel.Result {
    d.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
    display := models.GetDisplayByObjectId(d.MongoSession, id)
    display.Delete(d.MongoSession)
    return d.RenderJson(display)
}

/**
 * Create a display.
 */
func (d Display) Create() revel.Result {
    d.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")

    display, err := d.parseDisplay();
    if err != nil {
        return d.RenderText("Unable to parse the Display.")
    }
    display.Id = bson.NewObjectId()
    display.Save(d.MongoSession)
    return d.RenderJson(display)
}

/**
 * Update a specific display by id.
 */
func (d Display) Update(id string) revel.Result {
    d.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")

    display, err := d.parseDisplay();
    if err != nil {
        return d.RenderText("Unable to parse the Display.")
    }
    display.Id = bson.ObjectIdHex(id)
    display.Save(d.MongoSession)
    return d.RenderJson(display)
}

/**
 * Manage CROS preflight requests.
 */
func (d Display) Preflight() revel.Result {
    d.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
    d.Response.Out.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS")
    d.Response.Out.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    return d.RenderJson("ok")
}

/**
 * Parse a JSON Display model object from the request body.
 */
func (d Display) parseDisplay() (models.Display, error) {
    display := models.Display{}
    err := json.NewDecoder(d.Request.Body).Decode(&display)
    return display, err
}
