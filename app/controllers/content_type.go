package controllers

import (
    "github.com/revel/revel"
    models "bitbucket.org/hopsketch/hopsketch/app/models"
    "labix.org/v2/mgo/bson"
    "encoding/json"
)

type ContentType struct {
    App
}

/**
 * List all content types.
 */
func (ct ContentType) List() revel.Result {
    contentTypes := models.GetAllContentTypes(ct.MongoSession)
    ct.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
    return ct.RenderJson(contentTypes)
}

/**
 * Get a specific content type by id.
 */
func (ct ContentType) Get(id bson.ObjectId) revel.Result {
    if id.Hex() != "" {
        contentType := models.GetContentTypeByObjectId(ct.MongoSession, id)
        ct.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
        return ct.RenderJson(contentType)
    }
    return ct.NotFound("Invalid content type Id.")
}

/**
 * Delete a specific project by id.
 */
func (ct ContentType) Delete(id bson.ObjectId) revel.Result {
    ct.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
    contentType := models.GetContentTypeByObjectId(ct.MongoSession, id)

    // delete displays in this content type from mongo
    for _, display_id := range contentType.Ref_displays {
        display_object := models.GetDisplayByObjectId(ct.MongoSession, display_id)
        display_object.Delete(ct.MongoSession)
    }

    contentType.Delete(ct.MongoSession)
    return ct.RenderJson(contentType)
}

/**
 * Create a content type.
 */
func (ct ContentType) Create() revel.Result {
    ct.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")

    contentType, err := ct.parseContentType()
    if err != nil {
        return ct.RenderText("Unable to parse the Content Type.")
    }
    contentType.Id = bson.NewObjectId()
    for i := range contentType.Fields {
        contentType.Fields[i].Id = bson.NewObjectId()
    }
    contentType.Save(ct.MongoSession)
    return ct.RenderJson(contentType)
}

/**
 * Update a specific content type by id.
 */
func (ct ContentType) Update(id string) revel.Result {
    ct.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")

    contentType, err := ct.parseContentType()
    for i := range contentType.Fields {
        if !bson.IsObjectIdHex(contentType.Fields[i].Id.Hex()) {
            contentType.Fields[i].Id = bson.NewObjectId()
        }
    }
    if err != nil {
        return ct.RenderText("Unable to parse the Content Type.")
    }
    contentType.Id = bson.ObjectIdHex(id)
    contentType.Save(ct.MongoSession)
    return ct.RenderJson(contentType)
}

/**
 * Manage CROS preflight requests.
 */
func (ct ContentType) Preflight() revel.Result {
    ct.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
    ct.Response.Out.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS")
    ct.Response.Out.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    return ct.RenderJson("ok")
}

/**
 * Parse a JSON Content Type model object from the request body.
 */
func (ct ContentType) parseContentType() (models.ContentType, error) {
    contentType := models.ContentType{}
    err := json.NewDecoder(ct.Request.Body).Decode(&contentType)
    return contentType, err
}
