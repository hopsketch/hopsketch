package controllers

import (
    "github.com/revel/revel"
    models "bitbucket.org/hopsketch/hopsketch/app/models"
    "labix.org/v2/mgo/bson"
    "encoding/json"
)

type Project struct {
    App
}

/**
 * List all projects.
 */
func (p Project) List() revel.Result {
    projects := models.GetAllProjects(p.MongoSession)
    p.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
    return p.RenderJson(projects)
}

/**
 * Get a specific project by id.
 */
func (p Project) Get(id bson.ObjectId) revel.Result {
    if id.Hex() != "" {
        project := models.GetProjectByObjectId(p.MongoSession, id)
        p.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
        return p.RenderJson(project)
    }
    return p.NotFound("Invalid project Id.")
}

/**
 * Delete a specific project by id.
 */
func (p Project) Delete(id bson.ObjectId) revel.Result {
    p.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
    project := models.GetProjectByObjectId(p.MongoSession, id)

    // delete pages in this project from mongo
    for _, page_id := range project.Ref_pages {
        page_object := models.GetPageByObjectId(p.MongoSession, page_id)
        page_object.Delete(p.MongoSession)
    }

    // delete content types in this project from mongo
    for _, ct_id := range project.Ref_content_types {
        ct_object := models.GetContentTypeByObjectId(p.MongoSession, ct_id)
        ct_object.Delete(p.MongoSession)
    }

    // delete the project object from mongo
    project.Delete(p.MongoSession)
    return p.RenderJson(project)
}

/**
 * Create a project.
 */
func (p Project) Create() revel.Result {
    p.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")

    project, err := p.parseProject();
    if err != nil {
        return p.RenderText("Unable to parse the Project.")
    }
    project.Id = bson.NewObjectId()
    project.Save(p.MongoSession)
    return p.RenderJson(project)
}

/**
 * Update a specific project by id.
 */
func (p Project) Update(id string) revel.Result {
    p.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")

    project, err := p.parseProject();
    if err != nil {
        return p.RenderText("Unable to parse the Project.")
    }
    project.Id = bson.ObjectIdHex(id)
    project.Save(p.MongoSession)
    return p.RenderJson(project)
}

/**
 * Manage CROS preflight requests.
 */
func (p Project) Preflight() revel.Result {
    p.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
    p.Response.Out.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS")
    p.Response.Out.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    return p.RenderJson("ok")
}

/**
 * Parse a JSON Project model object from the request body.
 */
func (p Project) parseProject() (models.Project, error) {
    project := models.Project{}
    err := json.NewDecoder(p.Request.Body).Decode(&project)
    return project, err
}
