package controllers

import (
    "github.com/revel/revel"
    models "bitbucket.org/hopsketch/hopsketch/app/models"
    "labix.org/v2/mgo/bson"
    "encoding/json"
)

type Page struct {
    App
}

/**
 * List all page
 */
func (p Page) List() revel.Result {
    pages := models.GetAllPages(p.MongoSession)
    p.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
    return p.RenderJson(pages)
}

/**
 * Get a specific page by id.
 */
func (p Page) Get(id bson.ObjectId) revel.Result {
    if id.Hex() != "" {
        page := models.GetPageByObjectId(p.MongoSession, id)
        p.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
        return p.RenderJson(page)
    }
    return p.NotFound("Invalid Page Id.")
}

/**
 * Delete a specific page by id.
 */
func (p Page) Delete(id bson.ObjectId) revel.Result {
    p.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
    page := models.GetPageByObjectId(p.MongoSession, id)

    // delete content blocks in this page from mongo
    for _, cb_id := range page.Ref_content_blocks {
        cb_object := models.GetContentBlockByObjectId(p.MongoSession, cb_id)
        cb_object.Delete(p.MongoSession)
    }

    page.Delete(p.MongoSession)
    return p.RenderJson(page)
}

/**
 * Create a page.
 */
func (p Page) Create() revel.Result {
    p.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")

    page, err := p.parsePage();
    if err != nil {
        return p.RenderText("Unable to parse the Page.")
    }
    page.Id = bson.NewObjectId()
    page.Save(p.MongoSession)
    return p.RenderJson(page)
}

/**
 * Update a specific page by id.
 */
func (p Page) Update(id string) revel.Result {
    p.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")

    page, err := p.parsePage();
    if err != nil {
        return p.RenderText("Unable to parse the Page.")
    }
    page.Id = bson.ObjectIdHex(id)
    page.Save(p.MongoSession)
    return p.RenderJson(page)
}

/**
 * Manage CROS preflight requests.
 */
func (p Page) Preflight() revel.Result {
    p.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
    p.Response.Out.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS")
    p.Response.Out.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    return p.RenderJson("ok")
}

/**
 * Parse a JSON Page model object from the request body.
 */
func (p Page) parsePage() (models.Page, error) {
    page := models.Page{}
    err := json.NewDecoder(p.Request.Body).Decode(&page)
    return page, err
}
