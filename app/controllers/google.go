package controllers

import (
    "github.com/revel/revel"
    "net/http"
    "encoding/json"
    mgo "labix.org/v2/mgo"
    // "github.com/jgraham909/revmgo"
)

type Google struct {
    App
}

type (
    GoogleResult struct {
        State                  string   `json:"state"`
        Access_token           string   `json:"access_token"`
        Token_type             string   `json:"token_type"`
        Expires_in             string   `json:"expires_in"`
        Code                   string   `json:"code"`
        Scope                  string   `json:"scope"`
        Id_token               string   `json:"id_token"`
        Authuser               string   `json:"authuser"`
        Num_sessions           string   `json:"num_sessions"`
        Prompt                 string   `json:"prompt"`
        Session_state          string   `json:"session_state"`
        Client_id              string   `json:"client_id"`
        G_user_cookie_policy   string   `json:"g_user_cookie_policy"`
        Cookie_policy          string   `json:"cookie_policy"`
        Response_type          string   `json:"response_type"`
        Issued_at              string   `json:"issued_at"`
        Expires_at             string   `json:"expires_at"`
        Status                 Status
    }
    Status struct {
        Google_logged_in       bool     `json:"google_logged_in"`
        Signed_in              bool     `json:"signed_in"`
        Method                 string   `json:"method"`
    }
    ResponseBody struct {
        Issued_to              string   `json:"issued_to"`
        Audience               string   `json:"audience"`
        User_id                string   `json:"user_id"`
        Scope                  string   `json:"scope"`
        Expires_in             int      `json:"expires_in"`
        Email                  string   `json:"email"`
        Verified_email         bool     `json:"verified_email"`
        Access_type            string   `json:"access_type"`
    }
)

func (g Google) Connect() revel.Result {
    googleResult := GoogleResult{}
    err := json.NewDecoder(g.Request.Body).Decode(&googleResult)

    revel.INFO.Printf("Google result: %v", googleResult)
    if err != nil {
        revel.WARN.Printf("Error occured while processing the request body: %v error %v", g, err)
    }

    path := "https://www.googleapis.com/oauth2/v1/tokeninfo?access_token="
    path += googleResult.Access_token
    response, err := http.Get(path)

    if err != nil {
        revel.WARN.Printf("Error occured while getting the google token URL: %v error %v", g, err)
    }

    defer response.Body.Close()
    respBody := ResponseBody{}
    err = json.NewDecoder(response.Body).Decode(&respBody)

    revel.INFO.Printf("Google response: %v", respBody)
    if err != nil {
        revel.WARN.Printf("Error occured while processing the response body: %v error %v", g, err)
    }

    if err != nil {
        revel.WARN.Printf("Error occured while insert the user: %v error %v", g, err)
    }

    // create new user if necessary.
    user := mgo.User{}
    user.Username = respBody.Email
    user.Password = respBody.Email

    database := g.MongoSession.DB("hopsketch")
    database.UpsertUser(&user)

    g.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
    return g.RenderJson(respBody)
}

func (g Google) Disconnect() revel.Result {
    return g.RenderJson("disconnected")
}
