package controllers

import (
    "github.com/revel/revel"
    models "bitbucket.org/hopsketch/hopsketch/app/models"
    "labix.org/v2/mgo/bson"
    "encoding/json"
)

type User struct {
    App
}

/**
 * List all users.
 */
func (u User) List() revel.Result {
    users := models.GetAllUsers(u.MongoSession)
    u.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
    return u.RenderJson(users)
}

/**
 * Get a specific user by id.
 */
func (u User) Get(id bson.ObjectId) revel.Result {
    if id.Hex() != "" {
        user := models.GetUserByObjectId(u.MongoSession, id)
        u.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
        return u.RenderJson(user)
    }
    return u.NotFound("Invalid user Id.")
}

/**
 * Delete a specific user by id.
 */
func (u User) Delete(id bson.ObjectId) revel.Result {
    u.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
    user := models.GetUserByObjectId(u.MongoSession, id)

    // delete the user object from mongo
    user.Delete(u.MongoSession)
    return u.RenderJson(user)
}

/**
 * Create a user.
 */
func (u User) Create() revel.Result {
    u.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")

    user, err := u.parseUser();
    if err != nil {
        return u.RenderText("Unable to parse the User.")
    }
    user.Id = bson.NewObjectId()
    user.Save(u.MongoSession)
    return u.RenderJson(user)
}

/**
 * Update a specific user by id.
 */
func (u User) Update(id string) revel.Result {
    u.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")

    user, err := u.parseUser();
    if err != nil {
        return u.RenderText("Unable to parse the User.")
    }
    user.Id = bson.ObjectIdHex(id)
    user.Save(u.MongoSession)
    return u.RenderJson(user)
}

/**
 * Manage CROS preflight requests.
 */
func (u User) Preflight() revel.Result {
    u.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
    u.Response.Out.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS")
    u.Response.Out.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    return u.RenderJson("ok")
}

/**
 * Parse a JSON User model object from the request body.
 */
func (u User) parseUser() (models.User, error) {
    user := models.User{}
    err := json.NewDecoder(u.Request.Body).Decode(&user)
    return user, err
}
