'use strict';

/**
 * @ngdoc overview
 * @name hopsketchApp
 * @description
 * # hopsketchApp
 *
 * Main module of the application.
 */
angular
  .module('hopsketchApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'xeditable',
    'gridster',
    'placeholders',
    'directive.g+signin'
  ])
  .config(function($routeProvider, USER_ROLES) {
    $routeProvider
      .when('/', {
        templateUrl: 'public/views/project/project-list.html',
        controller: 'ProjectCtrl',
        data: {
          authorizedRoles: [USER_ROLES.admin, USER_ROLES.authenticated_user]
        }
      })
      .when('/project/create', {
        templateUrl: 'public/views/project/project-create.html',
        controller: 'ProjectCtrl',
        data: {
          authorizedRoles: [USER_ROLES.admin, USER_ROLES.authenticated_user]
        }
      })
      .when('/project/:projectId/info', {
        templateUrl: 'public/views/project/project.html',
        controller: 'ProjectCtrl',
        data: {
          authorizedRoles: [USER_ROLES.admin, USER_ROLES.authenticated_user]
        }
      })
      .when('/project/:projectId', {
        templateUrl: 'public/views/project/project.html',
        controller: 'ProjectCtrl',
        data: {
          authorizedRoles: [USER_ROLES.admin, USER_ROLES.authenticated_user]
        }
      })
      .when('/project/:projectId/content/create', {
        templateUrl: 'public/views/content-type/content-type.html',
        controller: 'ContentTypeCtrl',
        data: {
          authorizedRoles: [USER_ROLES.admin, USER_ROLES.authenticated_user]
        }
      })
      .when('/content/:contentTypeId', {
        templateUrl: 'public/views/content-type/content-type.html',
        controller: 'ContentTypeCtrl',
        data: {
          authorizedRoles: [USER_ROLES.admin, USER_ROLES.authenticated_user]
        }
      })
      .when('/project/:projectId/page/create', {
        templateUrl: 'public/views/page/page-info.html',
        controller: 'PageInfoCtrl',
        data: {
          authorizedRoles: [USER_ROLES.admin, USER_ROLES.authenticated_user]
        }
      })
      .when('/page/:pageId/info', {
        templateUrl: 'public/views/page/page-info.html',
        controller: 'PageInfoCtrl',
        data: {
          authorizedRoles: [USER_ROLES.admin, USER_ROLES.authenticated_user]
        }
      })
      .when('/page/:pageId', {
        templateUrl: 'public/views/page/page.html',
        controller: 'PageCtrl',
        data: {
          authorizedRoles: [USER_ROLES.admin, USER_ROLES.authenticated_user]
        }
      })
      .when('/login', {
        templateUrl: 'public/views/user/login.html',
        controller: 'LoginCtrl',
        data: {
          authorizedRoles: [USER_ROLES.authorizedRoles]
        }
      })
      .otherwise({
        redirectTo: '/'
      });
  });

angular.module('hopsketchApp')
  .run(function (editableOptions) {
    editableOptions.theme = 'bs3';
  });
