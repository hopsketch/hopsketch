'use strict';

/**
 * @ngdoc service
 * @name hopsketchApp.session
 * @description
 * # session
 * Service in the hopsketchApp.
 */
angular.module('hopsketchApp')
    .service('Session', function ($cookieStore) {
      /**
       * Create new Session object based on the user.
       *
       * @param sessionId
       * @param userId
       * @param userRole
       */
      this.create = function(userId, userRole, csrftoken) {
        // If any of the input argument is missing, intercept the login process.
        if (_.isUndefined(csrftoken) || _.isUndefined(userId) || _.isUndefined(userRole)) {
          $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
          return false;
        }

        this.userId = userId;
        this.userRole = userRole;
        this.csrftoken = csrftoken;

        // Store the XSRF-TOKEN into a cookie.
        $cookieStore.put('XSRF-TOKEN', csrftoken);
      };

      /**
       * Destroy the Session object.
       */
      this.destroy = function() {
        this.id = null;
        this.userId = null;
        this.userRole = null;

        // Destroy the XSRF-TOKEN cookie as well.
        $cookieStore.remove('XSRF-TOKEN');
      };
      return this;
    });
