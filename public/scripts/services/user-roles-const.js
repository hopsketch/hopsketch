'use strict';

/**
 * @ngdoc service
 * @name hopsketchApp.userRoles
 * @description
 * # userRoles
 * Constant in the hopsketchApp.
 */
angular.module('hopsketchApp')
  .constant('USER_ROLES', {
    all: '*',
    admin: 'admin',
    authenticated_user: 'authenticated user',
    anonymous_user: 'anonymous user'
  });
