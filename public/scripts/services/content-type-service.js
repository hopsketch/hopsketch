'use strict';

/**
 * @ngdoc service
 * @name hopsketchApp.contentType
 * @description
 * # contentType
 * Service in the hopsketchApp.
 */
angular.module('hopsketchApp')
  .factory('ContentType', ['$resource', function ($resource) {
      return $resource('http://localhost:9000/content_type/:Id');
    }]);
