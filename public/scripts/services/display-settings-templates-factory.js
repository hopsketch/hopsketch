'use strict';

/**
 * @ngdoc service
 * @name hopsketchApp.viewModeTemplate
 * @description
 * # viewModeTemplate
 * Factory in the hopsketchApp.
 */
angular.module('hopsketchApp')
  .factory('displaySettingsTemplates', function() {
      return {
        text: "<div ng-include src='\"views/field/text-view-mode.html\"'></div>",
        long_text: "<div ng-include src='\"views/field/long-text-view-mode.html\"'></div>",
        image: "<div ng-include src='\"views/field/image-view-mode.html\"'></div>",
        date: "<div ng-include src='\"views/field/date-view-mode.html\"'></div>"
      }
  });
