'use strict';

/**
 * @ngdoc service
 * @name hopsketchApp.contentBlock
 * @description
 * # contentBlock
 * Service in the hopsketchApp.
 */
angular.module('hopsketchApp')
  .factory('ContentBlock', ['$resource', function ($resource) {
      return $resource('http://localhost:9000/content_block/:Id');
    }]);
