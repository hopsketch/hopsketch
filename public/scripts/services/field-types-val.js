'use strict';

/**
 * @ngdoc service
 * @name hopsketchApp.fieldTypes
 * @description
 * # fieldTypes
 * Value in the hopsketchApp.
 */
angular.module('hopsketchApp')
  .value('fieldTypes', ['text', 'long_text', 'image', 'file', 'date']);
