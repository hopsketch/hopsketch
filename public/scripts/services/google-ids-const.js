'use strict';

/**
 * @ngdoc service
 * @name hopsketchApp.googleIds
 * @description
 * # googleIds
 * Constant in the hopsketchApp.
 */
angular.module('hopsketchApp')
  .constant('GOOGLE_IDS', {
    client_id: "980422251489-t4qlfrvjdmocth720nd9hpddlcr09goj.apps.googleusercontent.com"
  });
