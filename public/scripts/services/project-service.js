'use strict';

/**
 * @ngdoc service
 * @name hopsketchApp.Project
 * @description
 * # Project
 * Factory in the hopsketchApp.
 */
angular.module('hopsketchApp')
  .factory('Project', ['$resource', function ($resource) {
      return $resource('http://localhost:9000/project/:Id');
    }]);
