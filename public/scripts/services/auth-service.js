'use strict';

/**
 * @ngdoc service
 * @name hopsketchApp.AuthService
 * @description
 * # AuthService
 * Service in the hopsketchApp.
 */
angular.module('hopsketchApp')
    .factory('AuthService', function($http, $cookieStore, Session) {
      var authService = {};

      /**
       * Start the login process.
       *
       * @param credentials
       * @returns {*}
       */
      authService.login = function(credentials) {
        return $http
          .post('http://localhost:9000/connect', credentials)
          .then(function(res) {
            Session.create(res.data.id, res.data.user.id, res.data.user.role);
            return res.data.user;
          });
      };

      /**
       * Check if the user has successfully logged in.
       *
       * @returns {boolean}
       */
      authService.isAuthenticated = function() {
        var token = $cookieStore.get('XSRF-TOKEN');
        // User is authenticated if the session object has a userId set in and
        // the XSRF-TOKEN exists.
        if (!_.isUndefined(token) || Session.userId) {
          return true;
        }

        return false;
      };

      /**
       * Check that the user is authorized.
       *
       * @param authorizedRoles
       * @returns {boolean}
       */
      authService.isAuthorized = function(authorizedRoles) {
        if (!angular.isArray(authorizedRoles)) {
          authorizedRoles = [authorizedRoles];
        }
        return (authService.isAuthenticated() &&
          authorizedRoles.indexOf(Session.userRole) !== -1);
      };

      return authService;
    });
