'use strict';

/**
 * @ngdoc service
 * @name hopsketchApp.user
 * @description
 * # user
 * Service in the hopsketchApp.
 */
angular.module('hopsketchApp')
  .service('User', ['$resource', function ($resource) {
    return $resource('http://localhost:9000/user/:Id');
  }]);
