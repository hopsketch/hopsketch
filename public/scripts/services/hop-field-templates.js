'use strict';

/**
 * @ngdoc service
 * @name hopsketchApp.hopFieldTemplatees
 * @description
 * # hopFieldTemplatees
 * Service in the hopsketchApp.
 */
angular.module('hopsketchApp')
  .factory('hopFieldTemplates', function() {
      return {
        text: "<div ng-include src='\"views/field/text-display.html\"'></div>",
        long_text: "<div ng-include src='\"views/field/long-text-display.html\"'></div>",
        image: "<div ng-include src='\"views/field/image-display.html\"'></div>",
        date: "<div ng-include src='\"views/field/date-display.html\"'></div>"
      }
  });
