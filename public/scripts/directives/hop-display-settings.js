'use strict';

/**
 * @ngdoc directive
 * @name hopsketchApp.directive:hopViewMode
 * @description
 * # hopViewMode
 */
angular.module('hopsketchApp')
  .directive('hopDisplaySettings', function ($compile, displaySettingsTemplates) {
      console.log('asd');
      /**
       * Directive link function to link view mode templates
       * based on the type argument.
       *
       * @param scope
       * @param element
       * @param attrs
       */
      var linkHandler = function(scope, element, attrs) {
        // Get the field type.
        var type = scope.field.Type;
        // This is a bit ugly.
        var template = displaySettingsTemplates[type];
        console.log(template);
        // Configure the view mode template based on the field type.
        element.html(template).show();
        $compile(element.contents())(scope);
      };

      return {
        restrict: 'E',
        replace: true,
        link: linkHandler,
        scope: true
      };
  });
