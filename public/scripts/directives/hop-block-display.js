'use strict';

/**
 * This directive manage content blocks rendering.
 *
 * @ngdoc directive
 * @name hopsketchApp.directive:hopBlockDisplay
 * @description
 * # hopBlockDisplay
 */
angular.module('hopsketchApp')
  .directive('hopBlockDisplay', function ($compile, Display, ContentType, hopFieldTemplates) {
      /**
       * Directive link function to link view mode templates
       * based on the type argument.
       *
       * @param scope
       * @param element
       * @param attrs
       */
      var linkHandler = function(scope, element, attrs) {
        // Get the block display.
        var refDisplay = _.first(scope.block.Ref_display);

        if (!_.isUndefined(refDisplay)) {
          // Load the display object.
          Display.get({Id: refDisplay}, function(display) {
            // Load the content type object.
            ContentType.get({Id: display.Ref_content_type}, function(content) {
              if (scope.block.Settings.NumberOfElements >= 0) {
                // Render the displays according to the block configuration.
                for (var i = 1; i <= scope.block.Settings.NumberOfElements; i++) {
                  // Loop through the display fields.
                  _.each(display.Fields, function(value, index, list) {
                    var field = _.first(_.where(content.Fields, {Id: value.Ref_field}));
                    var template = hopFieldTemplates[field.Type];
                    // Attach the field html.
                    element.append(template).show();
                  });
                }
              }

              // Render the included templates (ng-include).
              $compile(element.contents())(scope);
            });
          });
        }
      };

      return {
        restrict: 'E',
        replace: true,
        link: linkHandler,
        scope: {
          block: "="
        }
      };
    });
