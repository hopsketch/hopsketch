'use strict';

/**
 * @ngdoc function
 * @name hopsketchApp.controller:ContenttypedisplayctrlCtrl
 * @description
 * # ContenttypedisplayctrlCtrl
 * Controller of the hopsketchApp
 */
angular.module('hopsketchApp')
  .controller('ContentTypeDisplayCtrl', function ($scope, $routeParams, ContentType, Display) {
      /**
       * The currently edited view.
       *
       * @type {{}}
       */
      $scope.activeDisplay = {};

      /**
       *
       *
       * @type {Array}
       */
      $scope.displayList = [];

      /**
       *
       * @type {{}}
       */
      $scope.newDisplay = {};

      /**
       * Load the given view field settings.
       *
       * @param $index
       */
      $scope.loadDisplay = function(index) {
        $scope.activeDisplay = $scope.displayList[index];
      };

      /**
       * Set the currently edited view as active.
       *
       * @param $index
       */
      $scope.editDisplayName = function(index) {
        $scope.activeDisplayName = index;
      };

      /**
       * Using this method to decide which view name should displayed as being edited.
       *
       * @param $index
       */
      $scope.isDisplayNameEdit = function(index) {
        return (index === $scope.activeDisplayName)
      };

      /**
       * After finished the edit of a view name, clear the activeDisplayName variable.
       */
      $scope.stopEditDispalyName = function() {
        $scope.activeDisplayName = null;
      };

      /**
       * Add a new view to the view list.
       */
      $scope.addNewDisplay = function(displayName) {
        if (!_.isEmpty(displayName)) {
          // Initialize the new display.
          var newDisplay = new Display();
          newDisplay.Name = displayName;
          newDisplay.Ref_content_type = $scope.contentData.Id;

          newDisplay.$save(function(display) {
            if (_.isNull($scope.contentData.Ref_displays)) {
              $scope.contentData.Ref_displays = [];
            }
            // Attach the display on the content type.
            $scope.contentData.Ref_displays.push(display.Id);
            $scope.contentData.$save({Id: $scope.contentData.Id});
            // Update the local display list.
            $scope.displayList.push(display);
            $scope.newDisplay = {};
            // Set the new display as the active display.
            $scope.activeDisplay = display;
          });
        }
      };

      /**
       * Attach a field onto the activeDisplay.
       *
       * @param index
       */
      $scope.attachFieldToDisplay = function(field) {
        var field_ref = {
          Ref_field: field.Id,
          Settings: {

          }
        };

        if (_.isUndefined($scope.activeDisplay.Fields)) {
          $scope.activeDisplay.Fields = [];
        }

        $scope.activeDisplay.Fields.push(field_ref);
        $scope.activeDisplay.$save({Id: $scope.activeDisplay.Id});
      };

      /**
       * Remove a field from the activeDisplay.
       *
       * @param index
       */
      $scope.removeFieldFromDisplay = function(field) {
        var fieldIndex = -1;

        _.each($scope.activeDisplay.Fields, function(element, index) {
          if (element.Ref_field === field.Id) {
            fieldIndex = index;
          }
        },field, fieldIndex);

        $scope.activeDisplay.Fields.splice(fieldIndex, 1);
        $scope.activeDisplay.$save({Id: field.Id});
      };

      /**
       * Check that a field is attached onto the activeDisplay.
       *
       * @param field
       * @returns {boolean}
       */
      $scope.isAttachedField = function(field) {
        var isAttached = false;

        _.each($scope.activeDisplay.Fields, function(element) {
          if (element.Ref_field === field.Id) {
            isAttached = true;
          }
        },field, isAttached);

        return isAttached;
      };

      /**
       * Remove a view from the view list.
       *
       * @param index
       */
      $scope.deleteDisplay = function(display) {
        var index = _.indexOf($scope.contentData.Ref_displays, $scope.displayList[display].Id);
        if (index > -1) {
          $scope.contentData.Ref_displays.splice(index, 1);
          $scope.contentData.$save({Id: $scope.contentData.Id});

          $scope.displayList[display].$delete({Id: $scope.displayList[display].Id});
          $scope.displayList.splice(display, 1);
        }
      };

      /**
       * Prepare scope variables.
       */
      $scope.init = function() {
        if (!_.isUndefined($routeParams.contentTypeId)) {
          ContentType.get({Id: $routeParams.contentTypeId}, function(content) {
            if (!_.isEmpty(content.Ref_displays)) {
              _.each(content.Ref_displays, function (Id) {
                Display.get({Id: Id}, function(display) {
                  $scope.displayList.push(display);

                  $scope.activeDisplay = display;
                });
              });
            }
          });
        }
      };

      //
      $scope.init();
  });
