'use strict';

/**
 * @ngdoc function
 * @name hopsketchApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the hopsketchApp
 */
angular.module('hopsketchApp')
  .controller('LoginCtrl', function ($scope, $rootScope, AUTH_EVENTS, GOOGLE_IDS, AuthService) {

      /**
       * Google client id.
       */
      $scope.client_id = GOOGLE_IDS.client_id;

      /**
       * Called after successful login request.
       */
      $scope.$on('event:google-plus-signin-success', function(event, authResult) {
        AuthService.login(authResult).then(function(user) {
          // Trigger the loginSuccess event.
          $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
          // Set the currentUser object.
          $scope.setCurrentUser(user);
          // Redirect the user on the project listing page.
          $location.path('/')
        });
      });

      /**
       * Called after failed login request.
       */
      $scope.$on('event:google-plus-signin-failure', function(event, authResult) {
        $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
      });
  });
