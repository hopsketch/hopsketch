'use strict';

/**
 * @ngdoc function
 * @name hopsketchApp.controller:HorizontalnavCtrl
 * @description
 * # HorizontalnavCtrl
 * Controller of the hopsketchApp
 */
angular.module('hopsketchApp')
  .controller('HorizontalPageNav', function ($scope, $routeParams, Project, Page, ContentType) {
      $scope.projectData = {};


      /**
       * Project pages.
       *
       * @type {Array}
       */
      $scope.horizontalPageNavList = [];

      /**
       * Load the given project pages by the project Id.
       */
      $scope.getProjectPages = function(projectId) {
        // Load the project object.
        Project.get({Id: projectId}, function(project) {
          $scope.projectData = project;
          // Check if the project has any pages.
          if (!_.isEmpty(project.Ref_pages)) {
            // Loop through the page references.

            _.each(project.Ref_pages, function(pageId) {
              //Load the page by Id.
              Page.get({Id: pageId}, function(page) {
                $scope.horizontalPageNavList.push(page);
              });
            });
          }
        });
      };

      /**
       * Init
       */
      $scope.init = function() {
        // Load project page list by pageId.
        if (!_.isUndefined($routeParams.pageId)) {
          Page.get({Id: $routeParams.pageId}, function(page) {
            if (!_.isEmpty(page.Ref_project)) {
              $scope.getProjectPages(page.Ref_project);
            }
          });
        }

        // Load project page list by projectId.
        if (!_.isUndefined($routeParams.projectId)) {
          $scope.getProjectPages($routeParams.projectId);
        }

        // Load project's page list by contentTypeId.
        if (!_.isUndefined($routeParams.contentTypeId)) {
          ContentType.get({Id: $routeParams.contentTypeId}, function(content) {
            if (!_.isEmpty(content.Ref_project)) {
              $scope.getProjectPages(content.Ref_project);
            }
          })
        }
      };

      $scope.init();

  });
