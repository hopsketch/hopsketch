'use strict';

/**
 * @ngdoc function
 * @name hopsketchApp.controller:ContentblockCtrl
 * @description
 * # ContentblockCtrl
 * Controller of the hopsketchApp
 */
angular.module('hopsketchApp')
  .controller('ContentBlockCtrl', function ($scope, $modal, ContentBlock) {
      /**
       * Content block data.
       */
      $scope.blockData = {};

      /**
       * Init instance variables for the block settings form.
       */
      $scope.instanceVariables = {
        contentTypeList: $scope.contentTypeList,
        project: $scope.projectData,
        blockData: $scope.blockData
      };

      /**
       * Delete content block entity and also remove the page reference.
       *
       * @param widget
       */
      $scope.remove = function(block) {
        ContentBlock.delete({Id: block.Id});
        var index = _.indexOf($scope.pageData.Ref_content_blocks, block.Id);
        if (index > -1) {
          $scope.pageData.Ref_content_blocks.splice(index, 1);
          $scope.pageData.$save({Id: $scope.pageData.Id});
        }
      };

      /**
       * Save the content block.
       */
      $scope.save = function() {
        ContentBlock.save({Id: $scope.blockData.Id}, $scope.blockData);
      };

      /**
       * Open the content block settings form.
       *
       * @param block
       */
      $scope.openSettings = function(block) {
        $scope.instanceVariables.blockData = block;

        var modalInstance = $modal.open({
          templateUrl: 'views/regions/content-block-settings.html',
          controller: 'ContentBlockSettingsCtrl',
          size: 'lg',
          resolve: {
            items: function () {
              return $scope.instanceVariables;
            }
          }
        });
      };

      /**
       * Init block.
       */
      $scope.init = function(block) {
        $scope.blockData = block;
      };

      $scope.init();
  });
