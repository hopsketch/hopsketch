'use strict';

/**
 * @ngdoc function
 * @name hopsketchApp.controller:ContentblocksettingsCtrl
 * @description
 * # ContentblocksettingsCtrl
 * Controller of the hopsketchApp
 */
angular.module('hopsketchApp')
  .controller('ContentBlockSettingsCtrl', function ($scope, $modalInstance, items, ContentType, Display, ContentBlock) {
      /**
       *
       * @type {Array|$scope.contentTypeList|*|string[]}
       */
      $scope.contentTypeList = items.contentTypeList;

      /**
       *
       * @type {*|$scope.projectData|{}}
       */
      $scope.projectData = items.projectData;

      /**
       *
       * @type {*|{}|$scope.instanceVariables.blockData}
       */
      $scope.blockData = items.blockData;

      /**
       *
       * @type {Array}
       */
      $scope.options = [];

      /**
       * Dismiss the modal instance.
       */
      $scope.dismiss = function() {
        $modalInstance.dismiss('cancel');
      };

      /**
       *
       */
      $scope.save = function() {
        _.each($scope.options, function(option) {
          if (option.BlockDisplay !== false) {
            $scope.blockData.Ref_display.push(option.BlockDisplay);
          }
        });

        ContentBlock.save({Id: $scope.blockData.Id}, $scope.blockData);
      };

      /**
       *
       */
      $scope.done = function() {
        $scope.save();
        $scope.dismiss();
      };

      /**
       * Initialize content block settings.
       */
      $scope.init = function() {
        // Get all the content types.
        _.each($scope.contentTypeList, function(content) {
          ContentType.get({Id: content.Id}, function(content) {
            var option = {
              Name: content.Name,
              Id: content.Id,
              BlockDisplay: false,
              Displays: []
            };

            // If the content type has any display get the data.
            if (!_.isEmpty(content.Ref_displays)) {
              _.each(content.Ref_displays, function(displayId) {
                Display.get({Id: displayId}, function (display) {
                  var displayOption = {
                    Id: display.Id,
                    Name: display.Name
                  };

                  // Check if the display.Id has enabled before.
                  var index = _.indexOf($scope.blockData.Ref_display, display.Id);
                  if (index > -1) {
                    option.BlockDisplay = display.Id;
                  }

                  option.Displays.push(displayOption);
                });
              }, option);

              $scope.options.push(option);
            }
          });
        });
      };

      /**
       *
       */
      $scope.init();
  });
