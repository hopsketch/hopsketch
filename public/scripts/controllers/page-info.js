'use strict';

/**
 * @ngdoc function
 * @name hopsketchApp.controller:CreatepageCtrl
 * @description
 * # CreatepageCtrl
 * Controller of the hopsketchApp
 */
angular.module('hopsketchApp')
  .controller('PageInfoCtrl', function ($scope, $routeParams, $location, Project, Page, ContentType) {

      /**
       *
       * @type {{}}
       */
      $scope.projectData = {};

      /**
       *
       * @type {{}}
       */
      $scope.pageData = {};

      /**
       *
       * @type {Array}
       */
      $scope.contentTypeList = [];

      /**
       *
       */
      $scope.save = function() {
        if (_.has($scope.pageData, 'Id')) {
          // Update
          $scope.updatePage();
        }
        else {
          // Create
          $scope.createPage();
        }
      };

      /**
       * Update the current page.
       */
      $scope.updatePage = function() {
        $scope.pageData.$save({Id: $scope.pageData.Id}, function() {
          $location.path('/page/' + $scope.pageData.Id);
        });
      };

      /**
       * Save a new page.
       */
      $scope.createPage = function() {
        $scope.pageData.$save(function(response, putResponseHeaders) {
          $scope.pageData.Id = response.Id;
          $scope.projectData.Ref_pages.push($scope.pageData.Id);
          $scope.projectData.$save({Id: $scope.projectData.Id});
          $location.path('/page/' + $scope.pageData.Id);
        });
      };

      /**
       * Delete the given page.
       */
      $scope.delete = function() {
        // At first delete the page entity.
        $scope.pageData.$delete({Id: $scope.pageData.Id});

        if (!_.isEmpty($scope.projectData.Ref_pages)) {
          // Also remove the page reference from the project Ref_pages.
          var index = $scope.projectData.Ref_pages.indexOf($scope.pageData.Id);
          if (index > -1) {
            $scope.projectData.Ref_pages.splice(index, 1);
            $scope.projectData.$save({Id: $scope.projectData.Id});
          }
        }

        $location.path('/project/' + $scope.projectData.Id + '/info');
      };

      /**
       * Initialize $scope data by pageID.
       *
       * @param pageId
       */
      $scope.initByPageId = function(pageId) {
        Page.get({Id: pageId}, function(page) {
          $scope.pageData = page;
          Project.get({Id: page.Ref_project}, function(project) {
            $scope.projectData = project;

            if (!_.isEmpty($scope.projectData.Ref_content_types)) {
              _.each($scope.projectData.Ref_content_types, function(contentId) {
                ContentType.get({Id: contentId}, function(content) {
                  $scope.contentTypeList.push({Name: content.Name, Id: content.Id});
                });
              });
            }
          });
        });
      };

      /**
       * Initialize $scope data by projectID.
       *
       * @param projectId
       */
      $scope.initByProjectId = function(projectId) {
        Project.get({Id: projectId}, function(project) {
          $scope.projectData = project;

          if (!_.isEmpty($scope.projectData.Ref_content_types)) {
            _.each($scope.projectData.Ref_content_types, function(contentId) {
              ContentType.get({Id: contentId}, function(content) {
                $scope.contentTypeList.push({Name: content.Name, Id: content.Id});
              });
            });
          }

          $scope.pageData.Ref_project = project.Id;
        });
      };

      /**
       *
       */
      $scope.init = function() {
        $scope.projectData = new Project();
        $scope.pageData = new Page();

        if (!_.isUndefined($routeParams.pageId)) {
          $scope.initByPageId($routeParams.pageId);
        }

        if (!_.isUndefined($routeParams.projectId)) {
          $scope.initByProjectId($routeParams.projectId);
        }
      };

      $scope.init();
  });
