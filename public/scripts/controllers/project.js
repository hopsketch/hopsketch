'use strict';

/**
 * @ngdoc function
 * @name hopsketchApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the hopsketchApp
 */
angular.module('hopsketchApp')
  .controller('ProjectCtrl', function ($scope, $sce, $routeParams, $location, Project, ContentType) {
      /**
       * List of projects.
       *
       * @type {Array}
       */
      $scope.projectList = Project.query();

      /**
       * Active project.
       *
       * @type {{}}
       */
      $scope.projectData = {};

      /**
       * The project content types.
       */
      $scope.contentTypeList = [];

      /**
       * Submit a new project.
       */
      $scope.save = function() {
        if (_.has($scope.projectData, 'Id')) {
          $scope.projectData.$save({Id: $scope.projectData.Id});
        }
        else {
          $scope.projectData.$save(function(response, putResponseHeaders) {
            $scope.projectData = response;
            $location.path('/project/' + $scope.projectData.Id + '/info');
          });
        }
      };

      /**
       * Delete a certain project.
       *
       * @param $index
       */
      $scope.delete = function($index) {
        $scope.projectData.$delete({Id: $scope.projectData.Id});
        $location.path('#');
      };

      /**
       * 
       */
      $scope.init = function() {
        var projectId = $routeParams.projectId;

        $scope.projectData = new Project();
        if (!_.isUndefined(projectId)) {
          Project.get({Id: projectId}, function(project) {
            $scope.projectData = project;

            if (!_.isEmpty($scope.projectData.Ref_content_types)) {
              _.each($scope.projectData.Ref_content_types, function(contentId) {
                ContentType.get({Id: contentId}, function(content) {
                  $scope.contentTypeList.push({Name: content.Name, Id: content.Id});
                });
              });
            }
          });
        }
      };

      /**
        * Init scope variables.
        */
       $scope.init();
    });
