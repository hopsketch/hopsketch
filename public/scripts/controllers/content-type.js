'use strict';

/**
 * @ngdoc function
 * @name hopsketchApp.controller:ContentTypeCtrl
 * @description
 * # ContentTypeCtrl
 * Controller of the hopsketchApp
 */
angular.module('hopsketchApp')
  .controller('ContentTypeCtrl', function ($scope, $routeParams, $location, Project, ContentType, fieldTypes) {

      /**
       * A flag that indicates that the page list region should be shown or hidden.
       *
       * @type {boolean}
       */
      $scope.showProjectPages = true;

      /**
       * Project pages.
       */
      $scope.pageList = [];

      /**
       * The project object. Load by the url param.
       *
       * @type {{}}
       */
      $scope.projectData = {};

      /**
       *
       * @type {{}}
       */
      $scope.contentData = {};

      /**
       * The project content types.
       */
      $scope.contentTypeList = [];

      /**
       * Available field types.
       *
       * @type {string[]}
       */
      $scope.fieldTypeList = fieldTypes;

      /**
       * Save the content type.
       */
      $scope.save = function() {
        // Update the content type.
        if (_.has($scope.contentData, 'Id')) {
          $scope.contentData.$save({Id: $scope.contentData.Id});
        }
        else {
          $scope.contentData.$save(function(content) {
            $scope.contentData.Id = content.Id;

            $scope.projectData.Ref_content_types.push($scope.contentData.Id);
            $scope.projectData.$save({Id: $scope.projectData.Id});

            // Redirect on the content type page.
            $location.path('/content/' + $scope.contentData.Id);
          });
        }
      };

      /**
       * Delete a given content type.
       */
      $scope.delete = function() {
        $scope.contentData.$delete({Id: $scope.contentData.Id});

        if (!_.isEmpty($scope.projectData.Ref_content_types)) {
          // Also remove the page reference from the project Ref_pages.
          var index = $scope.projectData.Ref_content_types.indexOf($scope.contentData.Id);
          if (index > -1) {
            $scope.projectData.Ref_content_types.splice(index, 1);
            $scope.projectData.$save({Id: $scope.projectData.Id});
          }
        }

        $location.path('/project/' + $scope.projectData.Id + '/info');
      };

      /**
       * Attach a field on the content type.
       *
       * @param fieldType
       */
      $scope.addField = function(fieldType) {
        var field = {
          Label: 'Field label',
          Description: 'Description',
          Type: fieldType
        };

        $scope.contentData.Fields.push(field);
        $scope.contentData.$save({Id: $scope.contentData.Id});
      };

      /**
       * Remove field from the content type.
       */
      $scope.removeField = function(index) {
        $scope.contentData.Fields.splice(index, 1);
        $scope.contentData.$save({Id: $scope.contentData.Id});
      };


      /**
       * Get the list of content types that belongs to the current project.
       */
      $scope.initContentList = function() {
        if (!_.isEmpty($scope.projectData.Ref_content_types)) {
          _.each($scope.projectData.Ref_content_types, function(contentId) {
            ContentType.get({Id: contentId}, function(content) {
              $scope.contentTypeList.push({Name: content.Name, Id: content.Id});
            });
          });
        }
      };

      /**
       * Controller object initialization.
       */
      $scope.init = function() {
        $scope.projectData = new Project();
        $scope.contentData = new ContentType();

        // In case of new content type creation.
        if (!_.isUndefined($routeParams.projectId)) {
          Project.get({Id: $routeParams.projectId}, function(project) {
            $scope.projectData = project;
            $scope.contentData.Ref_project = project.Id;
            // Get content type list.
            $scope.initContentList();
          });
        }

        // Start entity loading on a content type page.
        if (!_.isUndefined($routeParams.contentTypeId)) {
          ContentType.get({Id: $routeParams.contentTypeId}, function(content) {
            $scope.contentData = content;

            // Load the content type's project data.
            Project.get({Id: $scope.contentData.Ref_project}, function(project) {
              $scope.projectData = project;
              $scope.initContentList();
            });
          });
        }
      };

      /**
        * Init scope variables.
        */
       $scope.init();
  });
