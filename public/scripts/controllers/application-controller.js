'use strict';

/**
 * @ngdoc function
 * @name hopsketchApp.controller:ApplicationcontrollerCtrl
 * @description
 * # ApplicationcontrollerCtrl
 * Controller of the hopsketchApp
 */
angular.module('hopsketchApp')
    .controller('ApplicationController', function($scope, USER_ROLES, AuthService) {
      /**
       *
       *
       * @type {null}
       */
      $scope.currentUser = null;

      /**
       *
       */
      $scope.userRoles = USER_ROLES;

      /**
       *
       *
       * @type {*|Function}
       */
      $scope.isAuthorized = AuthService.isAuthorized;

      /**
       *
       * @param user
       */
      $scope.setCurrentUser = function(user) {
        $scope.currentUser = user;
      };
    });

/**
 *
 */
angular.module('hopsketchApp')
    .run(function($rootScope, AUTH_EVENTS, AuthService) {
      $rootScope.$on('$routeChangeStart', function (event, next) {
        var authorizedRoles = next.data.authorizedRoles;

        if (!AuthService.isAuthorized(authorizedRoles)) {
          event.preventDefault();

          if (AuthService.isAuthenticated()) {
            // user is not allowed
            $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
          } else {
            // user is not logged in
            $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
          }
        }
      })
    });
