'use strict';

/**
 * @ngdoc function
 * @name hopsketchApp.controller:PageCtrl
 * @description
 * # PageCtrl
 * Controller of the hopsketchApp
 */
angular.module('hopsketchApp')
  .controller('PageCtrl', function ($scope, $routeParams, $location, $modal, Project, Page, ContentBlock, ContentType) {
      /**
       * Flag to determine that the horizontal page listing visibility.
       *
       * @type {boolean}
       */
      $scope.showProjectPages = true;


      /**
       * Gridster configuration.
       *
       * @type {{resizable: {enabled: boolean}}}
       */
      $scope.gridsterConf = {
        columns: 12,
        resizable: {
           enabled: true
        }
      };

      /**
       * Map block configuration to gridster.
       *
       * @type {{}}
       */
      $scope.gridsterItemMap = {
        sizeX: 'block.Settings.Size[0]',
        sizeY: 'block.Settings.Size[1]',
        row: 'block.Settings.Position[0]',
        col: 'block.Settings.Position[1]'
      };

      /**
       * Page content blocks.
       *
       * @type {{sizeX: number, sizeY: number, row: number, col: number}[]}
       */
      $scope.pageBlocks = [];

      /**
       * Current page data.
       *
       * @type {{}}
       */
      $scope.pageData = {};

      /**
       * Current project data.
       *
       * @type {{}}
       */
      $scope.projectData = {};

      /**
       * Project content types.
       *
       * @type {string[]}
       */
      $scope.contentTypeList = [];

      /**
       * Available content displays.
       *
       * @type {Array}
       */
      $scope.viewModeList = [];


      /**
       * Add a content block to the page.
       */
      $scope.addContentBlock = function() {
        var blockData = new ContentBlock();
        blockData.Ref_pages = [$scope.pageData.Id];
        blockData.Settings = {
          Size: [4, 3],
          Position: [0, 0],
          ItemsPerRow: 1,
          NumberOfElements: 1,
          NumberOfRows: 1
        };

        blockData.$save(function(response) {
          $scope.pageData.Ref_content_blocks.push(response.Id);
          $scope.pageData.$save({Id: $scope.pageData.Id});
          $scope.pageBlocks.push(blockData);
        });
      };

      /**
       *
       *
       * @param block
       */
      $scope.savePageBlocks = function(block) {
        block.$save({Id: block.Id});
      };

      /**
       * Initialize $scope data by pageID.
       *
       * @param pageId
       */
      $scope.initByPageId = function(pageId) {
        Page.get({Id: pageId}, function(page) {
          $scope.pageData = page;
          // Load the referenced project.
          Project.get({Id: page.Ref_project}, function(project) {
            $scope.projectData = project;

            if (!_.isEmpty($scope.projectData.Ref_content_types)) {
              _.each($scope.projectData.Ref_content_types, function(contentId) {
                ContentType.get({Id: contentId}, function(content) {
                  $scope.contentTypeList.push({Name: content.Name, Id: content.Id});
                });
              });
            }
          });

          // Load page content blocks.
          if (!_.isEmpty($scope.pageData.Ref_content_blocks)) {
            // Loop through the block references.
            _.each($scope.pageData.Ref_content_blocks, function(blockRef) {
              // Load the block from by Id.
              ContentBlock.get({Id: blockRef}, function(response) {
                var updated = new Date();
                // Initialize the updated property.
                response.updated = updated.getTime();
                // Prepare the block for render.
                $scope.pageBlocks.push(response);
              });
            });
          }
        });
      };

      /**
       * Initialization.
       */
      $scope.init = function() {
        $scope.projectData = new Project();
        $scope.pageData = new Page();

        if (!_.isUndefined($routeParams.pageId)) {
          $scope.initByPageId($routeParams.pageId);
        }
      };

      $scope.init();
  });
